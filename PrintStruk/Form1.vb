﻿Imports System.Drawing.Printing
Imports System.IO
Imports System

Public Class Form1

    Public tmpBrcd(), tmpBrg(), tmpBeli(), tmpJual(), tmpQty(), tmpSat(), tmpHrg(), tmpFkl(), tmpJmlh(), tmpPot1(), tmpPot2(), tmpTot(), tmpTot2()
    Public tb As DataSet
    Public PT As String
    Public tbds As DataSet
    Public tmpjt As Date
    Public tmpnofak As String
    Public arrke As Integer
    Public printFont As Font

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        toprint()
        Me.Close()
    End Sub

    Sub toprint()

        arrke = 0
        printFont = New Font("Courier", 10)

        Dim PrintDoc As New PrintDocument
        AddHandler PrintDoc.PrintPage, AddressOf Me.Printnya1
        PrintDoc.Print()
    End Sub

    Dim linesPerPage As Single = 0
    Dim yPos As Single = 0
    Dim count As Integer = 0
    Dim line As String = Nothing

    Dim ukuran As New Font("Times New Romans", 11, FontStyle.Regular)
    Dim ukJD As New Font("Times New Romans", 9, FontStyle.Regular)
    Dim ukMn As New Font("Courier", 8, FontStyle.Regular)
    Dim ukMn2 As New Font("Courier", 8, FontStyle.Regular)
    Dim ukMn3 As New Font("Courier", 8.5, FontStyle.Regular)

    Public Sub Printnya1(ByVal sender As Object, ByVal ev As PrintPageEventArgs)

        ev.Graphics.DrawString("SLIP GAJI", New Font(FontFamily.GenericSerif, 12, FontStyle.Bold), Brushes.Black, 400, 10)
        ev.Graphics.DrawString("Periode : 25 Agustus - 25 September 2014", New Font(FontFamily.GenericSerif, 11, FontStyle.Bold), Brushes.Black, 300, 25)

        ev.Graphics.DrawString("Nama Karyawan", ukJD, Brushes.Black, 10, 55)
        ev.Graphics.DrawString(":", ukJD, Brushes.Black, 180, 55)
        ev.Graphics.DrawString("Agus", ukJD, Brushes.Black, 220, 55)

        ev.Graphics.DrawString("NIK", ukJD, Brushes.Black, 10, 70)
        ev.Graphics.DrawString(":", ukJD, Brushes.Black, 180, 70)
        ev.Graphics.DrawString("23432492", ukJD, Brushes.Black, 220, 70)

        ev.Graphics.DrawLine(Pens.Black, 0, 90, 400, 90)
        ev.Graphics.DrawString("Tanggal", ukJD, Brushes.Black, 5, 92)
        ev.Graphics.DrawString("Jam Kerja", ukJD, Brushes.Black, 65, 92)
        ev.Graphics.DrawString("Lembur", ukJD, Brushes.Black, 135, 92)
        ev.Graphics.DrawString("Status", ukJD, Brushes.Black, 195, 92)
        ev.Graphics.DrawString("Gaji Normal", ukJD, Brushes.Black, 245, 92)
        ev.Graphics.DrawString("Gaji Lembur", ukJD, Brushes.Black, 325, 92)
        ev.Graphics.DrawLine(Pens.Black, 0, 110, 400, 110)

        ev.Graphics.DrawLine(Pens.Black, 410, 90, 830, 90)
        ev.Graphics.DrawString("Tanggal", ukJD, Brushes.Black, 415, 92)
        ev.Graphics.DrawString("Jam Kerja", ukJD, Brushes.Black, 475, 92)
        ev.Graphics.DrawString("Lembur", ukJD, Brushes.Black, 545, 92)
        ev.Graphics.DrawString("Status", ukJD, Brushes.Black, 605, 92)
        ev.Graphics.DrawString("Gaji Normal", ukJD, Brushes.Black, 655, 92)
        ev.Graphics.DrawString("Gaji Lembur", ukJD, Brushes.Black, 735, 92)
        ev.Graphics.DrawLine(Pens.Black, 410, 110, 830, 110)

        Dim n As Integer = 0
        Dim count As Integer = 0
        For i = 0 To 31
            If n = 0 And i > 15 Then
                n = 400
                count = 0
            End If
            ev.Graphics.DrawString("1-Jan-2014", ukMn, Brushes.Black, 5 + n, 115 + ((count Mod 16) * 15))
            ev.Graphics.DrawString("8", ukMn, Brushes.Black, 115 + n, 115 + ((count Mod 16) * 15), New StringFormat(LeftRightAlignment.Right))
            ev.Graphics.DrawString("0", ukMn, Brushes.Black, 185 + n, 115 + ((count Mod 16) * 15), New StringFormat(LeftRightAlignment.Right))
            ev.Graphics.DrawString("Hadir", ukMn, Brushes.Black, 195 + n, 115 + ((count Mod 16) * 15))
            ev.Graphics.DrawString("Hadir", ukMn, Brushes.Black, 195 + n, 115 + ((count Mod 16) * 15))
            ev.Graphics.DrawString("0", ukMn, Brushes.Black, 325 + n, 115 + ((count Mod 16) * 15), New StringFormat(LeftRightAlignment.Right))
            ev.Graphics.DrawString("0", ukJD, Brushes.Black, 390 + n, 115 + ((count Mod 16) * 15), New StringFormat(LeftRightAlignment.Right))
            count += 1
        Next

        ev.Graphics.DrawLine(Pens.Black, 0, 343, 830, 343)
        ev.Graphics.DrawString("Gaji Pokok", ukJD, Brushes.Black, 10, 347)
        '  ev.Graphics.DrawString(":", ukJD, Brushes.Black, 200, 403)
        ev.Graphics.DrawString("2.293.846", ukJD, Brushes.Black, 380, 347, New StringFormat(LeftRightAlignment.Right))
        ev.Graphics.DrawLine(Pens.Black, 0, 365, 400, 365)

    End Sub
End Class
